package beeapp.user;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Cristina on 11/10/2016.
 */

@RestController
public class UserController {

    @RequestMapping(value = "/api/auth", method = RequestMethod.GET)
    public User user(@RequestParam(value = "username", defaultValue = "a") String usermane,
                     @RequestParam(value = "password", defaultValue = "a") String password,
                     @RequestParam(value = "token", defaultValue = "a") String token) {

        // va returna toti userii pe care ii are in DB, acum e doar hardcodat,
        // si va ramane hardcodat pana cand termin Androidul si Reactul -> login
        return new User(usermane, password, token);
    }

}
