package beeapp.user;

/**
 * Created by Cristina on 11/10/2016.
 */
public class User {
    private final String username;
    private final String password;
    private final String token;

    public User(String username, String password, String token) {
        this.username = username;
        this.password = password;
        this.token = token;
    }

    /**
     * !!!!!!!!!!!
     * Mandatory to write getters because
     * GET requets its
     * GET requests for /users and for this methods
     * !!!!!!!!!!!
     */
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }
}
