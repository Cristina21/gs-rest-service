package beeapp.task;

/**
 * Created by Cristina on 11/10/2016.
 */
public class Task {
    private final long id;
    private final String title;
    private final String details;

    public Task(long id, String title, String details) {
        this.id = id;
        this.title = title;
        this.details = details;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDetails() {
        return details;
    }
}
