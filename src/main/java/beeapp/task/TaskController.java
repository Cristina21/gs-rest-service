package beeapp.task;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Cristina on 11/10/2016.
 */

@RestController
public class TaskController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/api/task", method=RequestMethod.GET)
    public Task task(@RequestParam(value = "title", defaultValue = "theTitle") String title,
                     @RequestParam(value = "details", defaultValue = "theDetails") String details) {

        // va returna toate taskurile pe care le are in DB, acum e doar hardcodat,
        // si va ramane hardcodat pana cand termin Androidul si Reactul -> login
        return new Task(counter.incrementAndGet(), title, details);
    }

}
